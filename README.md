### RUN
## To run project :
- npm i 
- npm start
### Description :

## Goal is to create an OTT web application containing video content with the option of playback.

## Tech stack

# JS:
- React 
- React hooks

# CSS:
- css modules
- swiper

# Features:
- react-player
- react-router-dom
- react-lines-ellipsis
