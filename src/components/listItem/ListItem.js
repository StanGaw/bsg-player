import React, { useEffect } from "react";
import LinesEllipsis from "react-lines-ellipsis";
import { Link } from "react-router-dom";
import styles from "./styles.module.css";
import MoviePlaceholder from "../icons/MoviePlaceholder";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper-bundle.css";
const ListItem = ({ movies }) => {
  return (
    <Swiper
      slidesPerView={1}
      // centeredSlides={true}
      breakpoints={{
        400: {
          centeredSlides: true,
          slidesPerView: 1,
          // spaceBetween: 20
        },
        500: {
          centeredSlides: false,
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1000: {
          centeredSlides: false,
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1235: {
          centeredSlides: false,
          slidesPerView: 4,
          spaceBetween: 20,
        },
      }}
      className={styles.movieList}
    >
      {movies?.map((el) => {
        const cover = el.Images.find(
          (img) => (img.ImageTypeCode = "FRAME")
        )?.Url;

        return (
          <SwiperSlide className={styles.movieSlide}>
            <Link to={`movie/${el.Id}`} key={el.Id}>
              <li className={styles.movieList__item}>
                <div className={styles.title}>{el.Title}</div>
                <div className={styles.image}>
                  {cover !== undefined ? (
                    <img className={styles.img} src={cover} />
                  ) : (
                    <MoviePlaceholder />
                  )}
                </div>
                <div className={styles.descritpion}>
                  <LinesEllipsis
                    text={el.Description}
                    maxLine="4"
                    ellipsis="..."
                    trimRight
                    basedOn="letters"
                  />
                </div>
              </li>
            </Link>
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
};

export default ListItem;
