import React, { useEffect, useState, useRef } from "react";
import ReactPlayer from "react-player";
import Loader from "../loader/Loader";
import ArrowLeft from "../icons/ArrowLeft";
import { fetchMovie } from "../../functions/fetchData";
import { useParams, useHistory } from "react-router-dom";
import styles from "./styles.module.css";
const MoviePost = ({ token }) => {
  const { id } = useParams();
  const [currentMovie, setCurrentMovie] = useState();
  const [hasTrial, setTrial] = useState(true);
  const history = useHistory();
  useEffect(() => {
    fetchMovie(token, id).then((data) => setCurrentMovie(data));
  }, []);
  useEffect(() => {
    const checkIfHasUrl = setTimeout(() => {
      if (currentMovie?.ContentUrl === undefined) {
        setTrial(false);
      }
      return () => {
        clearTimeout(checkIfHasUrl);
      };
    }, 2000);
  }, [currentMovie]);
  const goBack = () => {
    history.push("/");
  };
  return (
    <div className={styles.wrapper}>
      <div onClick={() => goBack()} className={styles.arrowBack}>
        <ArrowLeft />
      </div>
      <div className={styles.container}>
        {currentMovie && currentMovie.ContentUrl !== undefined ? (
          <>
            <h2 className={styles.title}>{currentMovie.Title}</h2>
            <ReactPlayer
              controls={true}
              width="100%"
              height="100%"
              url={currentMovie.ContentUrl}
            />
          </>
        ) : hasTrial ? (
          <Loader />
        ) : (
          <h2 onClick={() => goBack()} className={styles.error}>
            <span className={styles.emoticonError}>:(</span>We are sorry but
            this content has no Trial version
            <br /> <span className={styles.goBack}>Go back To the List!</span>
          </h2>
        )}
      </div>
      <article>
        <h2>Description</h2>
        <p className={styles.description}>{currentMovie?.Description}</p>
      </article>
      ;
    </div>
  );
};

export default MoviePost;
