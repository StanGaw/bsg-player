import React from "react";
import styles from "./styles.module.css";
const Loader = () => <div className={styles.dotSpin}></div>;

export default Loader;
