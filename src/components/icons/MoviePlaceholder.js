import React from "react";

function MoviePlaceholder() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      version="1.1"
      viewBox="0 0 200 200"
    >
      <defs>
        <linearGradient id="linearGradient3095">
          <stop offset="0" stopColor="#b3b3b3"></stop>
          <stop offset="1" stopColor="#fff"></stop>
        </linearGradient>
        <linearGradient id="linearGradient3019">
          <stop offset="0" stopColor="#000"></stop>
          <stop offset="1" stopColor="#333"></stop>
        </linearGradient>
        <linearGradient
          id="linearGradient3115"
          x1="112.56"
          x2="112.56"
          y1="305.75"
          y2="42.234"
          gradientUnits="userSpaceOnUse"
          xlinkHref="#linearGradient3019"
        ></linearGradient>
        <linearGradient
          id="linearGradient3117"
          x1="73.75"
          x2="73.75"
          y1="129.87"
          y2="41.298"
          gradientUnits="userSpaceOnUse"
          xlinkHref="#linearGradient3019"
        ></linearGradient>
        <linearGradient
          id="linearGradient3119"
          x1="73.75"
          x2="73.75"
          y1="129.87"
          y2="41.298"
          gradientTransform="translate(0 -32.974)"
          gradientUnits="userSpaceOnUse"
          xlinkHref="#linearGradient3019"
        ></linearGradient>
        <linearGradient
          id="linearGradient3121"
          x1="110.5"
          x2="110.5"
          y1="70.659"
          y2="9.229"
          gradientUnits="userSpaceOnUse"
          xlinkHref="#linearGradient3095"
        ></linearGradient>
        <linearGradient
          id="linearGradient3123"
          x1="114.41"
          x2="114.41"
          y1="88.484"
          y2="53.484"
          gradientUnits="userSpaceOnUse"
          xlinkHref="#linearGradient3095"
        ></linearGradient>
      </defs>
      <g transform="translate(-17.623 36.573)">
        <g
          fillRule="evenodd"
          transform="translate(33.143 -5.452) scale(.72144)"
        >
          <path
            fill="url(#linearGradient3115)"
            d="M26.656 54.719v153.03h182.41V54.719H26.656z"
          ></path>
          <path
            fill="url(#linearGradient3117)"
            d="M26.66 54.741H209.07V87.715H26.66z"
          ></path>
          <g transform="rotate(-15 26.656 54.75)">
            <path
              fill="url(#linearGradient3119)"
              d="M26.66 21.767H209.07V54.741H26.66z"
            ></path>
            <path
              fill="url(#linearGradient3121)"
              d="M44.781 21.781L26.656 39.906V54.75h7.781l32.97-32.969H44.78zm39.969 0l-33 32.969h22.656l32.969-32.969H84.75zm39.938 0L91.719 54.75h22.625l32.969-32.969h-22.625zm39.938 0L131.657 54.75h22.656l32.969-32.969h-22.656zm39.969 0l-32.98 32.969h22.625l14.812-14.812V21.782h-4.469z"
            ></path>
          </g>
          <path
            fill="url(#linearGradient3123)"
            d="M26.656 54.75v14.812l18.156 18.156h22.625L34.469 54.75h-7.813zm25.156 0l32.969 32.969h22.625L74.434 54.75H51.809zm39.938 0l32.969 32.969h22.625l-32.96-32.969H91.759zm39.938 0l33 32.969h22.625L154.344 54.75h-22.656zm39.969 0l32.969 32.969h4.437V69.531l-14.78-14.781h-22.626z"
          ></path>
        </g>
      </g>
    </svg>
  );
}

export default MoviePlaceholder;
