const apiUrl = "https://thebetter.bsgroup.eu";

export async function fetchData(setLoading, data, setData, setToken) {
  const response = await fetch(apiUrl + "/Authorization/SignIn", {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    body: "{}",
  });
  const res = await response.json();

  let object = {
    MediaListId: 3,
    IncludeCategories: false,
    IncludeImages: true,
    IncludeMedia: false,
    PageNumber: 1,
    PageSize: 21,
  };
  object = JSON.stringify(object);
  const newresponse = await fetch(apiUrl + "/Media/GetMediaList", {
    method: "POST",
    body: object,
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + res.AuthorizationToken.Token,
    },
  });
  setToken(res.AuthorizationToken.Token);
  const mediaList = await newresponse.json();
  setData(mediaList.Entities);
  setLoading(true);
  return mediaList;
}

export async function fetchMovie(token, id) {
  let object = {
    MediaId: parseInt(id),
    StreamType: "TRIAL",
  };
  object = JSON.stringify(object);
  const newresponse = await fetch(apiUrl + "/Media/GetMediaPlayInfo", {
    method: "POST",
    body: object,
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token,
    },
  });
  const result = await newresponse.json();
  return result;
}

export default fetchMovie;
