import { useEffect, useState } from "react";
import ListItem from "./components/listItem/ListItem";
import Loader from "./components/loader/Loader";
import MoviePost from "./components/moviePost/MoviePost";
import { fetchData } from "./functions/fetchData";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
function App() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState();
  const [token, setToken] = useState();
  useEffect(() => {
    fetchData(setLoading, data, setData, setToken);
  }, []);

  return (
    <div className="App">
      <Router>
        {loading ? (
          <Switch>
            <Route exact path="/">
              <ListItem movies={data} />
              <ListItem movies={data} />
            </Route>
            <Route path="/movie/:id">
              <MoviePost token={token} />
            </Route>
          </Switch>
        ) : (
          <Loader />
        )}
      </Router>
    </div>
  );
}

export default App;
